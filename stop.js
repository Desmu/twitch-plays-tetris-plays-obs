import OBSWebSocket from "obs-websocket-js";
import OBSWebSocketServer from "./server/OBSWebSocketServer.js";
import Utils from "./server/Utils.js";

/** Fichier de configuration global. */
let configuration = Utils.pathToFile("../configuration.js");
configuration = JSON.parse(configuration.substring(configuration.indexOf("=") + 1));

/** Instance d'OBSWebSocket. */
const obs = new OBSWebSocket();
obs.connect({"address": configuration.obs.wsAddress, "password": configuration.obs.wsPassword}).then(async () => {
    const tmpSettings = Utils.fileToJSON("../settings.tmp.json");
    await obs.send("SetCurrentScene", {
        "scene-name": tmpSettings.scene
    }).catch((error) => {
        console.log(error);
    });
    OBSWebSocketServer.stopGame(configuration, obs);
});

