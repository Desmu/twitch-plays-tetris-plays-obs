import fs from "fs";

import Utils from "./server/Utils.js";
import HTTPServer from "./server/HTTPServer.js";
import WebSocketServer from "./server/WebSocketServer.js";
import OBSWebSocketServer from "./server/OBSWebSocketServer.js";
import TwitchIRC from "./server/TwitchIRC.js";

/** Fichier de configuration inter-scripts contenant l'identifiant du processus du script de démarrage, puis le nom de la scène dupliquée. */
const tmpSettingsFile = Utils.pathSolver("../settings.tmp.json");
fs.writeFileSync(tmpSettingsFile, JSON.stringify({"pid": process.pid}), "utf8");

/** Fichier de configuration global. */
let configuration = Utils.pathToFile("../configuration.js");
configuration = JSON.parse(configuration.substring(configuration.indexOf("=") + 1));

/** Instance de gestion du serveur HTTP servant les fichiers statiques du client web. */
const httpServer = new HTTPServer(configuration, () => {
    /** Instance de gestion du serveur WebSocket recevant les mises à jour du client web (lignes supprimées, game over) et renvoyant les instructions du chat Twitch. */
    const webSocketServer = new WebSocketServer(configuration, () => {
    /** Instance de gestion d'OBSWebSocket gérant les interactions avec les scènes et sources OBS. */
        const obsWebSocketServer = new OBSWebSocketServer(configuration, () => {
            webSocketServer.onMessage(configuration, obsWebSocketServer, (ws) => {
                /** Instance de gestion du chat Twitch recevant les messages du chat. */
                const twitchIRC = new TwitchIRC(configuration, ws);
            });
        });
    });
});

process.on("error", (error) => {
    console.log(error);
});

