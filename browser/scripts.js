/** Connexion au serveur WebSocket du jeu. */
const ws = new WebSocket(`ws://${configuration.ws.domain}:${configuration.ws.port}`);
/** Mise en cache des éléments SVG des blocs. */
const tetrominoes = {};

/**
 * Test de collisions du bloc courant avec un autre bloc. S'il s'agit d'un bloc venant d'être placé, c'est qu'il n'est plus possible de jouer.
 *
 * @param {boolean} isFirstBlock true si le bloc vient d'être placé sur la grille.
 *
 * @returns {boolean} true si la collision est effective.
 */
function tcollision (isFirstBlock) {
    let isCollided = false;
    document.querySelectorAll(".current rect").forEach((currentSquare) => {
        document.querySelectorAll("rect").forEach((testingSquare) => {
            if ((currentSquare !== testingSquare)
             && (currentSquare.getBoundingClientRect().x === testingSquare.getBoundingClientRect().x)
             && (currentSquare.getBoundingClientRect().y === testingSquare.getBoundingClientRect().y)) {
                isCollided = true;
            }
        });
    });
    if (isFirstBlock && isCollided) {
        ws.send(JSON.stringify({"gameover": true}));
    }
    return isCollided;
}

/**
 * Placement d'un nouveau bloc sur la grille.
 *
 * @param {string} lastBlockName Nom du précédent bloc courant.
 */
function tpush (lastBlockName) {
    let block = "";
    let newLetter = Object.keys(tetrominoes)[Math.floor(Math.random() * Object.keys(tetrominoes).length)];
    if (lastBlockName) {
        while (newLetter === lastBlockName) {
            newLetter = Object.keys(tetrominoes)[Math.floor(Math.random() * Object.keys(tetrominoes).length)];
        }
    }
    block = tetrominoes[newLetter];
    document.querySelector("#main").innerHTML += `<g class="current">${block}</g>`;
    document.querySelector(".current g").setAttribute("transform", "rotate(0)");
    document.querySelector(".current").setAttribute("transform", `scale(${configuration.game.size}) translate(${((configuration.game.width / 2) - (configuration.game.size * 2)) / configuration.game.size}, 0)`);
    tcollision(true);
}

/**
 * Bloc courant arrivé au sol ou sur une pile de blocs, élimination des lignes si possible.
 */
function tfinish () {
    const [currentBlockName] = document.querySelector(".current g").classList;
    document.querySelectorAll(".current rect").forEach((square) => {
        const scale = document.querySelector("svg").createSVGTransform();
        const translate = document.querySelector("svg").createSVGTransform();
        scale.setScale(configuration.game.size, configuration.game.size);
        translate.setTranslate(square.getBoundingClientRect().x / configuration.game.size, square.getBoundingClientRect().y / configuration.game.size);
        square.transform.baseVal.appendItem(scale);
        square.transform.baseVal.appendItem(translate);
        square.removeAttribute("x");
        square.removeAttribute("y");
        document.querySelector("svg").appendChild(square);
    });
    document.querySelector(".current").remove();
    const squaresByLines = {};
    document.querySelectorAll("rect").forEach((square) => {
        if (square.getBoundingClientRect().y < configuration.game.height) {
            if (squaresByLines[square.getBoundingClientRect().y]) {
                squaresByLines[square.getBoundingClientRect().y] += 1;
            } else {
                squaresByLines[square.getBoundingClientRect().y] = 1;
            }
        }
    });
    const indexesRemoved = [];
    Object.entries(squaresByLines).forEach(([index, squaresByLine]) => {
        if ((squaresByLine * configuration.game.size) === configuration.game.width) {
            document.querySelectorAll("rect").forEach((square) => {
                if (square.getBoundingClientRect().y === parseInt(index, 10)) {
                    square.remove();
                    if (indexesRemoved.indexOf(parseInt(index, 10)) === -1) {
                        indexesRemoved.push(parseInt(index, 10));
                    }
                }
            });
            document.querySelectorAll("rect").forEach((square) => {
                if (square.getBoundingClientRect().y < parseInt(index, 10)) {
                    const transformBaseVal = square.transform.baseVal;
                    for (let i = 0; i < transformBaseVal.numberOfItems; i += 1) {
                        if (transformBaseVal.getItem(i).type === SVGTransform.SVG_TRANSFORM_TRANSLATE) {
                            const x = transformBaseVal.getItem(i).matrix.e;
                            let y = transformBaseVal.getItem(i).matrix.f;
                            y += 1;
                            transformBaseVal.getItem(i).setTranslate(parseInt(x, 10), parseInt(y, 10));
                            break;
                        }
                    }
                }
            });
        }
    });
    if (indexesRemoved.length > 0) {
        ws.send(JSON.stringify({indexesRemoved}));
    }
    tpush(currentBlockName);
}

/**
 * Replacement du bloc en cas de collision.
 */
function treplace () {
    let x = 0;
    let y = 0;
    for (let i = 0; i < document.querySelector(".current").transform.baseVal.numberOfItems; i += 1) {
        if (document.querySelector(".current").transform.baseVal.getItem(i).type === SVGTransform.SVG_TRANSFORM_TRANSLATE) {
            x = document.querySelector(".current").transform.baseVal.getItem(i).matrix.e;
            y = document.querySelector(".current").transform.baseVal.getItem(i).matrix.f;
            while ((document.querySelector(".current").getBoundingClientRect().y + document.querySelector(".current").getBoundingClientRect().height) > configuration.game.height) {
                y -= 1;
                document.querySelector(".current").transform.baseVal.getItem(i).setTranslate(parseInt(x, 10), parseInt(y, 10));
            }
            while ((document.querySelector(".current").getBoundingClientRect().x + document.querySelector(".current").getBoundingClientRect().width) > configuration.game.width) {
                x -= 1;
                document.querySelector(".current").transform.baseVal.getItem(i).setTranslate(parseInt(x, 10), parseInt(y, 10));
            }
            while (document.querySelector(".current").getBoundingClientRect().x < 0) {
                x += 1;
                document.querySelector(".current").transform.baseVal.getItem(i).setTranslate(parseInt(x, 10), parseInt(y, 10));
            }
            break;
        }
    }
}

/**
 * Déplacement du bloc courant.
 *
 * @param {string} direction Sens de déplacement ("up", "left", "down" ou "right" ; "up" provoque la chute instantanée).
 */
function tmove (direction) {
    for (let i = 0; i < document.querySelector(".current").transform.baseVal.numberOfItems; i += 1) {
        if (document.querySelector(".current").transform.baseVal.getItem(i).type === SVGTransform.SVG_TRANSFORM_TRANSLATE) {
            let x = document.querySelector(".current").transform.baseVal.getItem(i).matrix.e;
            let y = document.querySelector(".current").transform.baseVal.getItem(i).matrix.f;
            let isCollided = false;
            switch (direction) {
            case "up":
                while ((document.querySelector(".current").getBoundingClientRect().y + document.querySelector(".current").getBoundingClientRect().height) < configuration.game.height) {
                    y += 1;
                    document.querySelector(".current").transform.baseVal.getItem(i).setTranslate(parseInt(x, 10), parseInt(y, 10));
                    isCollided = tcollision(false);
                    if (isCollided) {
                        y -= 1;
                        document.querySelector(".current").transform.baseVal.getItem(i).setTranslate(parseInt(x, 10), parseInt(y, 10));
                        break;
                    }
                }
                tfinish();
                break;
            case "right":
                if ((document.querySelector(".current").getBoundingClientRect().x + document.querySelector(".current").getBoundingClientRect().width) <= configuration.game.width) {
                    x += 1;
                    document.querySelector(".current").transform.baseVal.getItem(i).setTranslate(parseInt(x, 10), parseInt(y, 10));
                    treplace();
                    isCollided = tcollision(false);
                    if (isCollided) {
                        x -= 1;
                        document.querySelector(".current").transform.baseVal.getItem(i).setTranslate(parseInt(x, 10), parseInt(y, 10));
                    }
                }
                break;
            case "down":
                if ((document.querySelector(".current").getBoundingClientRect().y + document.querySelector(".current").getBoundingClientRect().height) < configuration.game.height) {
                    y += 1;
                    document.querySelector(".current").transform.baseVal.getItem(i).setTranslate(parseInt(x, 10), parseInt(y, 10));
                    treplace();
                    isCollided = tcollision(false);
                    if (isCollided) {
                        y -= 1;
                        document.querySelector(".current").transform.baseVal.getItem(i).setTranslate(parseInt(x, 10), parseInt(y, 10));
                        tfinish();
                    }
                } else {
                    tfinish();
                }
                break;
            case "left":
                if (document.querySelector(".current").getBoundingClientRect().x >= 0) {
                    x -= 1;
                    document.querySelector(".current").transform.baseVal.getItem(i).setTranslate(parseInt(x, 10), parseInt(y, 10));
                    treplace();
                    isCollided = tcollision(false);
                    if (isCollided) {
                        x += 1;
                        document.querySelector(".current").transform.baseVal.getItem(i).setTranslate(parseInt(x, 10), parseInt(y, 10));
                    }
                }
                break;
            default:
                break;
            }
            break;
        }
    }
}

/**
 * Basculement du bloc courant à 90 degrés.
 *
 * @param {string} direction Sens de rotation ("left" ou "right").
 */
function tspin (direction) {
    for (let i = 0; i < document.querySelector(".current g").transform.baseVal.numberOfItems; i += 1) {
        if (document.querySelector(".current g").transform.baseVal.getItem(i).type === SVGTransform.SVG_TRANSFORM_ROTATE) {
            let isCollided = false;
            let {angle} = document.querySelector(".current g").transform.baseVal.getItem(i);
            switch (direction) {
            case "left":
                angle -= 90;
                document.querySelector(".current g").transform.baseVal.getItem(i).setRotate(angle, 1, 1);
                treplace();
                isCollided = tcollision(false);
                if (isCollided) {
                    angle += 90;
                    document.querySelector(".current g").transform.baseVal.getItem(i).setRotate(angle, 1, 1);
                }
                break;
            case "right":
                angle += 90;
                document.querySelector(".current g").transform.baseVal.getItem(i).setRotate(angle, 1, 1);
                treplace();
                isCollided = tcollision(false);
                if (isCollided) {
                    angle -= 90;
                    document.querySelector(".current g").transform.baseVal.getItem(i).setRotate(angle, 1, 1);
                }
                break;
            default:
                break;
            }
            break;
        }
    }
}

/**
 * Point d'entrée : récupération des SVG, redimensionnement de la grille, initialisation de l'intervalle de mouvement.
 */
ws.onopen = () => {
    const tetrominoesLetters = ["i", "j", "l", "o", "s", "t", "z"];
    const tetrominoesBasePath = `http://${configuration.http.domain}:${configuration.http.port}/tetrominoes/${configuration.game.theme}/`;
    const tetrominoesPaths = [];
    tetrominoesLetters.forEach((tetromino) => {
        tetrominoesPaths.push(`${tetrominoesBasePath}${tetromino}.svg`);
    });
    Promise.all(tetrominoesPaths.map((tetrominoesPath) => fetch(tetrominoesPath).then((resp) => resp.text()))).then((tetrominoesSVG) => {
        tetrominoesSVG.forEach((tetrominoSVG) => {
            const svgElem = document.createElementNS("http://www.w3.org/2000/svg", "g");
            svgElem.innerHTML = tetrominoSVG;
            tetrominoes[svgElem.firstElementChild.firstElementChild.classList[0]] = svgElem.firstElementChild.innerHTML;
        });
        document.querySelector("#main").setAttribute("height", configuration.game.height);
        document.querySelector("#main").setAttribute("width", configuration.game.width);
        tpush(null);
        setInterval(() => {
            tmove("down");
        }, configuration.game.speed);
    });
};

/**
 * Réception d'une commande du chat Twitch par l'intermédiaire du serveur WebSocket.
 *
 * @param {object} message Requête reçue.
 */
ws.onmessage = (message) => {
    const data = JSON.parse(message.data);
    switch (data.key) {
    case "up":
        tmove("up");
        break;
    case "right":
        tmove("right");
        break;
    case "down":
        tmove("down");
        break;
    case "left":
        tmove("left");
        break;
    case "spinleft":
        tspin("left");
        break;
    case "spinright":
        tspin("right");
        break;
    default:
        break;
    }
};

