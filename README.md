# Twitch Plays Tetris Plays OBS

[Vidéo de présentation](https://peertube.desmu.fr/w/kCZjZXmvmCxtNUDYK6Y3KV)

Jeu non officiel permettant à un chat Twitch de jouer à Tetris qui lui-même joue avec des scènes OBS.  
D'après une idée originale de [AvecUnHache](https://twitter.com/avecunhache).

## Description

Le script duplique une scène OBS donnée (ou la scène courante) dans une nouvelle scène, découpe les sources s'y trouvant en bandes horizontales, y superpose un Tetris, et lie le Tetris à un chat Twitch.  
Les personnes présentes dans le chat Twitch peuvent ensuite contrôler la partie de Tetris ; si une ligne est faite, les sources se trouvant à la même hauteur que la ligne réalisée sont supprimées, et celles situées plus haut sont déplacées vers le bas.  
La partie se termine en cas de victoire (toutes les sources dupliquées ont été supprimées), de défaite (plus aucun bloc ne peut être ajouté au Tetris sans provoquer de collision), ou d'arrêt volontaire du script.  
Il est possible de spécifier des scènes particulières à afficher après une victoire ou une défaite (sinon, le script réaffiche la scène précédente).

## Pré-requis

- Un compte [Twitch](https://www.twitch.tv/).
- Le logiciel [OBS](https://obsproject.com/).
- Le plugin [obs-websocket](https://github.com/Palakis/obs-websocket).
- [Node.js](https://nodejs.org) en version 13 ou supérieure.

## Installation

- [Télécharger le projet](https://gitlab.com/Desmu/twitch-plays-tetris-plays-obs/-/archive/master/twitch-plays-tetris-plays-obs-master.zip).
- Dans le dossier du projet, installer les dépendances externes avec l'une des options suivantes :
  * Exécuter le fichier `install.js` avec le programme `Node.js` (par défaut situé à l'emplacement `C:\Program Files\nodejs\node.exe` sur Windows, s'il n'est pas affiché dans la liste des programmes disponibles).
  * Exécuter la commande `npm install --prod` dans le dossier du programme depuis le terminal.
- Renommer le fichier `configuration.example.js` en `configuration.js` et en modifier les paramètres (voir paragraphe `Configuration`) via un éditeur de texte autre que le Bloc-Notes (Atom, Notepad++, Visual Studio, Sublime Text, ...).

## Lancement

- Ouvrir OBS, puis démarrer le jeu avec l'une des options suivantes :
  * Exécuter le fichier `start.js` avec le programme `Node.js`.
  * Exécuter la commande `node start.js` dans le dossier du programme depuis le terminal.
- Couper le jeu manuellement avec l'une des options suivantes :
  * Exécuter le fichier `stop.js` avec le programme `Node.js`.
  * Exécuter la commande `node stop.js` dans le dossier du programme depuis le terminal.

## Configuration

La plupart des paramètres peuvent être laissés tels quels, à l'exception de `obs.wsAddress` et `obs.wsPassword` s'ils ont été changés (sur OBS, ces paramètres sont situés dans `Outils > Paramètres du serveur WebSockets`), et des paramètres du bloc `twitch`.  
Le mot de passe OAuth à spécifier dans `twitch.userPass` se génère via cette page https://twitchapps.com/tmi/ , il s'agit de la suite de chiffres et de lettres situées après la partie `oauth:`.

`commands` : Commandes utilisées sur le chat Twitch pour interagir avec le bloc courant du Tetris.
- `up` : Provoque la chute du bloc ("!haut").
- `right` : Déplace le bloc vers la droite ("!droite").
- `down` : Déplace le bloc vers le bas ("!bas").
- `left` : Déplace le bloc vers la gauche ("!gauche").
- `spinleft` : Pivote le bloc de 90° vers la gauche ("!spingauche").
- `spinright` : Pivote le bloc de 90° vers la droite ("!spindroite").

`twitch` : Paramètres liés au compte Twitch utilisé pour se connecter au chat.
- `userId` : Identifiant du compte Twitch en minuscules.
- `userPass` : Mot de passe OAuth du compte Twitch.

`game` : Paramètres liés au Tetris.
- `height` : Hauteur du jeu en pixels (1080).
- `size` : Taille d'un carré composant un bloc en pixels (54).
- `speed` : Vitesse de déplacement du bloc courant en millisecondes (1000).
- `theme` : Nom du dossier comportant les fichiers SVG des blocs ("default").
- `width` : Largeur du jeu en pixels (540).

`obs` : Paramètres liés à OBS et OBSWebSocket.
- `sceneName` : Nom de la scène créée dans OBS par le programme pour afficher le jeu ("TwitchPlaysTetrisPlaysOBS").
- `sourceName` : Nom de la source créée dans OBS par le programme pour afficher le Tetris ("GameTwitchPlaysTetrisPlaysOBS").
- `wsAddress` : Domaine et port du serveur WebSocket utilisé par OBSWebSocket ("localhost:4444").
- `wsPassword` : Mot de passe du serveur WebSocket utilisé par OBSWebSocket.
- `sceneDuplicated` : Nom de la scène à dupliquer, laisser vide pour dupliquer la scène courante.
- `losingScene` : Nom de la scène à afficher en cas de défaite (les blocs du Tetris atteignent le sommet), laisser vide pour réafficher la scène courante.
- `winningScene` : Nom de la scène à afficher en cas de victoire (la scène dupliquée ne contient plus aucune source initialement dupliquée), laisser vide pour réafficher la scène courante.
- `protectedSourcePrefix` : Préfixe du nom des sources qui seront préservées sur la scène de jeu ("PROTECTED").

`http` : Paramètres liés au serveur HTTP utilisé pour récupérer les fichiers du Tetris.
- `domain` : Domaine du serveur ("localhost").
- `port` : Port du serveur (1956).

`ws` : Paramètres liés au serveur WebSocket utilisé pour communiquer entre les différentes parties.
- `domain` : Domaine du serveur ("localhost").
- `port` : Port du serveur (1984).

Pour un meilleur confort de jeu, il est recommandé de paramétrer les dimensions de la grille en fonction de la taille d'un carré de bloc.  
Pour une hauteur de grille `game.height` de 1080 pixels comportant 20 lignes, la taille d'un carré de bloc `game.size` sera donc de 54 pixels (1080 / 20), et une largeur de grille `game.width` comportant 10 colonnes sera de 540 pixels (54 * 10).  
Pour une hauteur de grille `game.height` de 720 pixels comportant 20 lignes, la taille d'un carré de bloc `game.size` sera donc de 36 pixels (720 / 20), et une largeur de grille `game.width` comportant 10 colonnes sera de 360 pixels (36 * 10).

Il est possible de révéler une ou plusieurs sources protégées au fil de la complétion de la partie (ces sources sont cachées sous les sources dupliquées et révélées au fur et à mesure de la suppression des sources dupliquées). Il suffit pour cela d'ajouter ces sources protégées à la scène du jeu (ayant pour nom la valeur spécifiée dans `obs.sceneName`) en précédant le nom de ces sources par la valeur spécifiée dans `obs.protectedSourcePrefix`.
Avec les valeurs de configuration par défaut, en ajoutant une image nommée `PROTECTED_Image` à la scène `TwitchPlaysTetrisPlaysOBS` (qu'il est possible de créer manuellement), cette image sera cachée sous le jeu et affichée au fil de la suppression des éléments de la scène dupliquée.

## Librairies utilisées

- [obs-websocket-js](https://github.com/haganbmj/obs-websocket-js) pour la liaison à OBSWebSocket.
- [ws](https://github.com/websockets/ws) pour la création du serveur WebSocket.
