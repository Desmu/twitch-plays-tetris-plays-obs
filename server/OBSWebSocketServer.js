import fs from "fs";
import OBSWebSocket from "obs-websocket-js";
import Utils from "./Utils.js";

/**
 * Gestion d'OBS.
 */
export default class OBSWebSocketServer {
    /**
   * Arrêt du jeu, suppression de toutes les sources de la scène du jeu, arrêt des scripts.
   *
   * @param {object} configuration Configuration du jeu.
   * @param {object} obs Instance d'OBSWebSocket.
   */
    static stopGame (configuration, obs) {
        obs.send("GetSceneItemList", {
            "sceneName": configuration.obs.sceneName
        }).then(async (responseGSIL) => {
            const promises = [];
            responseGSIL.sceneItems.forEach((sceneItem) => {
                if (sceneItem.sourceName.indexOf(configuration.obs.protectedSourcePrefix) !== 0) {
                    promises.push(obs.send("DeleteSceneItem", {
                        "scene": configuration.obs.sceneName,
                        "item": {
                            "id": sceneItem.itemId
                        }
                    }).catch((error) => {
                        console.log(error);
                    }));
                }
            });
            Promise.all(promises).catch((error) => {
                console.log(error);
            });
            const tmpSettings = Utils.fileToJSON("../settings.tmp.json");
            fs.unlinkSync(Utils.pathSolver("../settings.tmp.json"));
            process.kill(tmpSettings.pid, "SIGTERM");
            if (process.pid !== tmpSettings.pid) {
                process.kill(process.pid, "SIGTERM");
            }
        }).catch((error) => {
            console.log(error);
        });
    }

    /**
   * Connexion à OBS, duplication et découpage de la scène sur laquelle jouer, ajout du client web du jeu.
   *
   * @param {object} configuration Configuration du jeu.
   * @param {Function} callback Fonction de retour à la connexion à OBS.
   */
    constructor (configuration, callback) {
    /** Instance d'OBSWebSocket. */
        this.obs = new OBSWebSocket();
        this.obs.connect({"address": configuration.obs.wsAddress, "password": configuration.obs.wsPassword}).then(async () => {
            callback();
            this.obs.send("GetSceneList").then(async (responseGSL) => {
                if (configuration.obs.sceneDuplicated === "") {
                    configuration.obs.sceneDuplicated = responseGSL["current-scene"];
                }
                const tmpSettings = Utils.fileToJSON("../settings.tmp.json");
                tmpSettings.scene = configuration.obs.sceneDuplicated;
                fs.writeFileSync(Utils.pathSolver("../settings.tmp.json"), JSON.stringify(tmpSettings), "utf8");
                let promises = [
                    this.obs.send("GetVideoInfo").catch((error) => {
                        console.log(error);
                    }),
                    this.obs.send("GetSceneItemList", {"sceneName": configuration.obs.sceneDuplicated}).catch((error) => {
                        console.log(error);
                    })
                ];
                const existingScene = responseGSL.scenes.some((scene) => {
                    if (scene.name === configuration.obs.sceneName) {
                        return true;
                    }
                    return false;
                });
                if (existingScene) {
                    promises.push(this.obs.send("GetSceneItemList", {"sceneName": configuration.obs.sceneName}).catch((error) => {
                        console.log(error);
                    }));
                } else {
                    promises.push(this.obs.send("CreateScene", {"sceneName": configuration.obs.sceneName}).catch((error) => {
                        console.log(error);
                    }));
                }
                await Promise.all(promises).then(async (responses) => {
                    promises = [];
                    const videoWidth = responses[0].baseWidth;
                    responses[1].sceneItems.forEach((sceneItem) => {
                        promises.push(this.obs.send("GetSceneItemProperties", {
                            "scene-name": configuration.obs.sceneDuplicated,
                            "item": {
                                "id": sceneItem.itemId
                            }
                        }).catch((error) => {
                            console.log(error);
                        }));
                    });
                    let protectedScenes = [];
                    if (responses[2].sceneItems && responses[2].sceneItems.length > 0) {
                        protectedScenes = responses[2].sceneItems;
                    }
                    await Promise.all(promises).then(async (responsesGSIP) => {
                        promises = [];
                        const scenesProperties = [];
                        responsesGSIP.forEach((responseGSIP) => {
                            const startY = responseGSIP.position.y;
                            const sceneProperties = responseGSIP;
                            delete sceneProperties.messageId;
                            delete sceneProperties["message-id"];
                            delete sceneProperties["request-type"];
                            sceneProperties["scene-name"] = configuration.obs.sceneName;
                            if ((responseGSIP.height > configuration.game.size) || (parseInt(responseGSIP.position.y / configuration.game.size, 10) !== parseInt((responseGSIP.position.y + responseGSIP.height) / configuration.game.size, 10))) {
                                const firstLineHeight = configuration.game.size - (responseGSIP.position.y % configuration.game.size);
                                const lastLineHeight = (responseGSIP.position.y + responseGSIP.height) % configuration.game.size;
                                let linesCount = parseInt((responseGSIP.height - firstLineHeight - lastLineHeight) / configuration.game.size, 10);
                                if (firstLineHeight > 0) {
                                    linesCount += 1;
                                }
                                if (lastLineHeight > 0) {
                                    linesCount += 1;
                                }
                                for (let i = 0; i < linesCount; i += 1) {
                                    let top = (configuration.game.size * i) / sceneProperties.scale.y;
                                    let bottom = (configuration.game.size * (linesCount - i - 1)) / sceneProperties.scale.y;
                                    if (firstLineHeight > 0) {
                                        if (i === 0) {
                                            top = 0;
                                        } else if (i === 1) {
                                            sceneProperties.position.y = startY + firstLineHeight;
                                            top = firstLineHeight / sceneProperties.scale.y;
                                        } else {
                                            sceneProperties.position.y = startY + configuration.game.size * (i - 1) + firstLineHeight;
                                            top = (firstLineHeight + (configuration.game.size * (i - 1))) / sceneProperties.scale.y;
                                        }
                                    } else {
                                        sceneProperties.position.y = startY + configuration.game.size * i;
                                    }
                                    if (lastLineHeight > 0) {
                                        if ((i + 1) === linesCount) {
                                            bottom = 0;
                                        } else if ((i + 2) === linesCount) {
                                            bottom = lastLineHeight / sceneProperties.scale.y;
                                        } else {
                                            bottom = (lastLineHeight + (configuration.game.size * (linesCount - i - 2))) / sceneProperties.scale.y;
                                        }
                                    }
                                    sceneProperties.crop = {top, bottom};
                                    if (sceneProperties.position.y < configuration.game.height) {
                                        scenesProperties.push(JSON.parse(JSON.stringify(sceneProperties)));
                                        promises.push(this.obs.send("DuplicateSceneItem", {
                                            "fromScene": configuration.obs.sceneDuplicated,
                                            "toScene": configuration.obs.sceneName,
                                            "item": {
                                                "id": responseGSIP.itemId
                                            }
                                        }).catch((error) => {
                                            console.log(error);
                                        }));
                                    }
                                }
                            } else if (sceneProperties.position.y < configuration.game.height) {
                                scenesProperties.push(JSON.parse(JSON.stringify(sceneProperties)));
                                promises.push(this.obs.send("DuplicateSceneItem", {
                                    "fromScene": configuration.obs.sceneDuplicated,
                                    "toScene": configuration.obs.sceneName,
                                    "item": {
                                        "id": responseGSIP.itemId
                                    }
                                }).catch((error) => {
                                    console.log(error);
                                }));
                            }
                        });
                        await Promise.all(promises).then(async (responsesDSI) => {
                            promises = [];
                            const scenesOrder = [];
                            protectedScenes.forEach((protectedScene) => {
                                scenesOrder.push({"id": protectedScene.itemId});
                            });
                            responsesDSI.forEach((responseDSI) => {
                                let sP = null;
                                for (let j = 0; j < scenesProperties.length; j += 1) {
                                    if (scenesProperties[j].name === responseDSI.item.name) {
                                        sP = {...scenesProperties[j]};
                                        delete sP.itemId;
                                        delete sP.name;
                                        sP.item = responseDSI.item;
                                        scenesProperties.splice(j, 1);
                                        break;
                                    }
                                }
                                if (sP) {
                                    promises.push(this.obs.send("SetSceneItemProperties", JSON.parse(JSON.stringify(sP))).catch((error) => {
                                        console.log(error);
                                    }));
                                    scenesOrder.push({"id": responseDSI.item.id});
                                }
                            });
                            await Promise.all(promises).then(() => {
                                this.obs.send("ReorderSceneItems", {
                                    "scene": configuration.obs.sceneName,
                                    "items": scenesOrder.reverse()
                                }).then(() => {
                                    this.obs.send("CreateSource", {
                                        "sceneName": configuration.obs.sceneName,
                                        "sourceName": configuration.obs.sourceName,
                                        "sourceKind": "browser_source",
                                        "sourceSettings": {
                                            "url": `http://${configuration.http.domain}:${configuration.http.port}/index.html`,
                                            "height": configuration.game.height,
                                            "width": configuration.game.width
                                        }
                                    }).then((responseCS) => {
                                        this.obs.send("SetSceneItemProperties", {
                                            "scene-name": configuration.obs.sceneName,
                                            "item": {
                                                "id": responseCS.itemId
                                            },
                                            "position": {
                                                "x": (videoWidth - configuration.game.width) / 2
                                            }
                                        }).then(() => {
                                            this.obs.send("SetCurrentScene", {
                                                "scene-name": configuration.obs.sceneName
                                            }).catch((error) => {
                                                console.log(error);
                                            });
                                        }).catch((error) => {
                                            console.log(error);
                                        });
                                    }).catch((error) => {
                                        console.log(error);
                                    });
                                }).catch((error) => {
                                    console.log(error);
                                });
                            }).catch((error) => {
                                console.log(error);
                            });
                        }).catch((error) => {
                            console.log(error);
                        });
                    }).catch((error) => {
                        console.log(error);
                    });
                }).catch((error) => {
                    console.log(error);
                });
            }).catch((error) => {
                console.log(error);
            });
        }).catch((error) => {
            console.log(error);
        });
    }

    /**
   * Suppression des éléments de la scène de jeu d'après les coordonnées des lignes supprimées dans le client web du jeu, déplacement des éléments situés plus en hauteur, déclaration de la victoire si la scène est vide.
   *
   * @param {object} configuration Configuration du jeu.
   * @param {Array} indexesRemoved Tableau des coordonnées Y des lignes supprimées.
   */
    removeIndexes (configuration, indexesRemoved) {
        this.obs.send("GetSceneItemList", {
            "sceneName": configuration.obs.sceneName
        }).then(async (responseGSIL) => {
            let promises = [];
            responseGSIL.sceneItems.forEach((sceneItem) => {
                if ((sceneItem.sourceName !== configuration.obs.sourceName) && (sceneItem.sourceName.indexOf(configuration.obs.protectedSourcePrefix) !== 0)) {
                    promises.push(this.obs.send("GetSceneItemProperties", {
                        "scene-name": configuration.obs.sceneName,
                        "item": {
                            "id": sceneItem.itemId
                        }
                    }).catch((error) => {
                        console.log(error);
                    }));
                }
            });
            await Promise.all(promises).then(async (responsesGSIP) => {
                promises = [];
                const deletedItems = [];
                const movedItems = {};
                responsesGSIP.forEach((responseGSIP) => {
                    if (deletedItems.indexOf(responseGSIP.itemId) === -1) {
                        indexesRemoved.forEach((indexRemoved) => {
                            if (responseGSIP.position.y === indexRemoved) {
                                deletedItems.push(responseGSIP.itemId);
                                promises.push(this.obs.send("DeleteSceneItem", {
                                    "scene": configuration.obs.sceneName,
                                    "item": {
                                        "id": responseGSIP.itemId
                                    }
                                }).catch((error) => {
                                    console.log(error);
                                }));
                            } else if ((responseGSIP.position.y < indexRemoved) && (indexesRemoved.indexOf(responseGSIP.position.y) === -1)) {
                                if (!movedItems[responseGSIP.itemId]) {
                                    movedItems[responseGSIP.itemId] = responseGSIP.position.y;
                                }
                                movedItems[responseGSIP.itemId] += configuration.game.size;
                            }
                        });
                    }
                });
                Object.entries(movedItems).forEach(([itemId, newY]) => {
                    promises.push(this.obs.send("SetSceneItemProperties", {
                        "scene-name": configuration.obs.sceneName,
                        "item": {
                            "id": parseInt(itemId, 10)
                        },
                        "position": {
                            "y": newY
                        }
                    }).catch((error) => {
                        console.log(error);
                    }));
                });
                if (deletedItems.length === responsesGSIP.length) {
                    let sceneName = "";
                    if (configuration.obs.winningScene === "") {
                        sceneName = configuration.obs.sceneDuplicated;
                    } else {
                        sceneName = configuration.obs.winningScene;
                    }
                    promises.push(this.obs.send("SetCurrentScene", {
                        "scene-name": sceneName
                    }).catch((error) => {
                        console.log(error);
                    }));
                    OBSWebSocketServer.stopGame(configuration, this.obs);
                }
            });
        }).catch((error) => {
            console.log(error);
        });
    }

    /**
   * Fin du jeu par défaite au client web du jeu.
   *
   * @param {object} configuration Configuration du jeu.
   */
    gameOver (configuration) {
        console.log(configuration.obs.sceneDuplicated);
        let sceneName = "";
        if (configuration.obs.losingScene === "") {
            sceneName = configuration.obs.sceneDuplicated;
        } else {
            sceneName = configuration.obs.losingScene;
        }
        this.obs.send("SetCurrentScene", {
            "scene-name": sceneName
        }).catch((error) => {
            console.log(error);
        });
        OBSWebSocketServer.stopGame(configuration, this.obs);
    }
}

