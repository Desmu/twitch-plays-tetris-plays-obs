import fs from "fs";
import http from "http";
import Utils from "./Utils.js";

/**
 * Gestion du serveur HTTP.
 */
export default class HTTPServer {
    /**
   * Initialisation du serveur. Normalisation des URL, redirections vers les fichiers statiques.
   *
   * @param {object} configuration Configuration du jeu.
   * @param {Function} callback Fonction de retour à l'initialisation du serveur.
   */
    constructor (configuration, callback) {
    /** Instance du serveur HTTP. */
        this.server = http.createServer((req, res) => {
            let reqUrl = req.url.
                replace(/&/gu, "&amp;").
                replace(/"/gu, "&quot;").
                replace(/'/gu, "&#039;").
                replace(/</gu, "&lt;").
                replace(/>/gu, "&gt;");
            if (reqUrl.indexOf("configuration.js") === -1) {
                reqUrl = `../browser${reqUrl}`;
            } else {
                reqUrl = `../${reqUrl}`;
            }
            const filePath = Utils.pathSolver(reqUrl);
            if (fs.existsSync(filePath)) {
                fs.readFile(filePath, "utf8", (err, data) => {
                    const [, ext] = filePath.split(".");
                    let mimeType = "";
                    switch (ext) {
                    case "html": case "css":
                        mimeType = `text/${ext}`;
                        break;
                    case "js":
                        mimeType = "application/javascript";
                        break;
                    case "svg":
                        mimeType = "image/svg+xml";
                        break;
                    default:
                        mimeType = "text/plain";
                        break;
                    }
                    res.writeHead(200, {"Content-Type": mimeType});
                    res.write(data);
                    res.end();
                });
            } else {
                res.writeHead(400, {"Content-Type": "application/json"});
                res.write(JSON.stringify({}));
                res.end();
            }
        }).listen({"port": configuration.http.port}, callback);
    }
}

