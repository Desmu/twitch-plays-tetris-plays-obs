import WebSocket from "ws";

/**
 * Gestion du chat Twitch.
 */
export default class TwitchIRC {
    /**
     * Connexion au chat Twitch, récupération des messages correspondant à des commandes de jeu.
     *
     * @param {object} configuration Configuration du jeu.
     * @param {object} ws Connexion WebSocket du client web du jeu.
     */
    constructor (configuration, ws) {
        const tws = new WebSocket("wss://irc-ws.chat.twitch.tv:443");
        tws.onopen = () => {
            tws.send(`PASS oauth:${configuration.twitch.userPass}`);
            tws.send(`NICK ${configuration.twitch.userId}`);
            tws.send(`JOIN #${configuration.twitch.userId}`);
        };
        tws.onmessage = (message) => {
            if (message.data === "PING :tmi.twitch.tv") {
                tws.send("PONG :tmi.twitch.tv");
            } else if (message.data.indexOf("PRIVMSG") !== -1) {
                Object.entries(configuration.commands).some(([index, command]) => {
                    if (message.data.indexOf(command) !== -1) {
                        ws.send(JSON.stringify({"key": index}));
                        return true;
                    }
                    return false;
                });
            }
        };
    }
}

