import WebSocket from "ws";

/**
 * Gestion du serveur WebSocket.
 */
export default class WebSocketServer {
    /**
     * Initialisation du serveur.
     *
     * @param {object} configuration Configuration du jeu.
     * @param {Function} callback Fonction de retour à l'initialisation du serveur.
     */
    constructor (configuration, callback) {
    /** Instance du serveur WebSocket. */
        this.server = new WebSocket.Server({"port": configuration.ws.port});
        this.server.on("listening", () => {
            callback();
        });
    }

    /**
     * Réception d'un message via le client web du jeu.
     *
     * @param {object} configuration Configuration du jeu.
     * @param {object} obsWebSocketServer Objet contenant l'instance d'OBSWebSocket.
     * @param {Function} callback Fonction de retour à l'initialisation du serveur.
     */
    onMessage (configuration, obsWebSocketServer, callback) {
        this.server.on("connection", (ws) => {
            ws.on("message", (message) => {
                const data = JSON.parse(message);
                if (data.indexesRemoved && data.indexesRemoved.length > 0) {
                    obsWebSocketServer.removeIndexes(configuration, data.indexesRemoved);
                } else if (data.gameover) {
                    obsWebSocketServer.gameOver(configuration);
                }
            });
            callback(ws);
        });
    }
}

