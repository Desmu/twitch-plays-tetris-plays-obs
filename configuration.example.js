const configuration = {
    "commands": {
        "up": "!haut",
        "right": "!droite",
        "down": "!bas",
        "left": "!gauche",
        "spinleft": "!spingauche",
        "spinright": "!spindroite"
    },
    "twitch": {
        "userId": "streamername",
        "userPass": "0123456789abcdef"
    },
    "game": {
        "height": 1080,
        "size": 54,
        "speed": 1000,
        "theme": "default",
        "width": 540
    },
    "obs": {
        "sceneName": "TwitchPlaysTetrisPlaysOBS",
        "sourceName": "GameTwitchPlaysTetrisPlaysOBS",
        "wsAddress": "localhost:4444",
        "wsPassword": "",
        "sceneDuplicated": "",
        "losingScene": "",
        "winningScene": "",
        "protectedSourcePrefix": "PROTECTED"
    },
    "http": {
        "domain": "localhost",
        "port": 1956
    },
    "ws": {
        "domain": "localhost",
        "port": 1984
    }
}

